import {StyleSheet} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import OnBoarding from './Pages/onBoarding';
import MainPage from './Pages/MainPage';
import RecipeDetail from './Pages/RecipeDetail';
import Footer from './Components/Footer/Footer';
import ModePage from './Pages/Mode';
import Event from './Pages/CreateAnEvent';
const Stack = createNativeStackNavigator();
const App = (props)=> {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={OnBoarding} />
          <Stack.Screen name="Mode" component={ModePage} />
          <Stack.Screen name="MainPage" component={MainPage} />
          <Stack.Screen name="RecipeDetails" component={RecipeDetail} />
          <Stack.Screen name="Event" component={Event} />
        </Stack.Navigator>
      </NavigationContainer>
      
    </>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  gif:{
    height:300,
    width:300, 
    marginBottom:50
  }
});
export default App