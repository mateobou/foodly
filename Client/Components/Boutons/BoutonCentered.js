import { StatusBar } from 'expo-status-bar';
import { Button, Pressable, StyleSheet, Text, View } from 'react-native';
export default function BoutonCentre({onClick,title="Get Started"}) {
  return (
    <Pressable color={"white"} style={styles.button} onPress={()=>onClick()}>
      <Text style={styles.color}>{title}</Text>
    </Pressable>
  );
}
const styles = StyleSheet.create({
  button: {
    backgroundColor:"#292D32",
    justifyContent:'center',
    alignItems:'center',
    height:50,
    width:150,
    borderRadius:10,
  },
  color:{
      color:'white'
  }
});
