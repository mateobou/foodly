import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function Category({content, status}) {
  return (
    <View  style={status === "selected" ? styles.CategoryeSelected : styles.category}>
        <Text style={status === "selected" ? styles.titleSelected : styles.title}>{content}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  category:{
    margin:5,
    borderRadius:10,
    shadowColor:'black',
    shadowOpacity:0.1,
    shadowRadius:20, 
    backgroundColor:'white',
    padding:10
  },
  titleSelected:{
    color:'white'
  },

  CategoryeSelected:{
    margin:5,
    borderRadius:10,
    shadowColor:'black',
    shadowOpacity:0.1,
    shadowRadius:20, 
    backgroundColor:'white',
    padding:10,
    backgroundColor:'#f72585',
    paddingHorizontal:15
  },
  title:{
      color:'black',
      fontSize:15,
      fontWeight:'bold',
      textAlign:'center'
  }
});
