import { LinearGradient } from 'expo-linear-gradient';
import { StatusBar } from 'expo-status-bar';
import { useEffect } from 'react';
import { Dimensions, Image, StyleSheet, Text, View } from 'react-native';
import StartCooking from './StartCooking';
const vw = Dimensions.get('window').width/100;
const vh = Dimensions.get('window').height/100;
export default function CookingEmbed({image, title}) {

    return (
        <View style={styles.bigContainer}>
            <Image style={styles.Container} source={{uri:image}} blurRadius={5} resizeMode="stretch"/>
            <LinearGradient colors={["#e60c5b","#ffffff"]} style={{...styles.Container, opacity:0.4, position:'absolute', top:0}}>
            </LinearGradient>
            <View style={styles.ContainerText}>
                <Text style={styles.Title}>{title}</Text>
                <Text style={styles.text}>Chef Matéo</Text>
                <StartCooking/>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
  Container:{
      height: 33*vh,
      width:80*vw,
      backgroundColor:'white',
      borderRadius:15,
      display:'flex',
      justifyContent:'center',
      alignItems:'center',
      marginTop:30
  },
  ContainerText:{
    height: 35*vh,
    width:80*vw,
    display:'flex',
    alignItems:'center',
    justifyContent:'center',
    textAlign:'center',
    position:'relative',
    top:-35*vh, 
    textAlign:'center'

},
  Title:{
      fontSize:22,
      color:'white',
      fontWeight:"bold",
      textAlign:'center',
      width:"80%"
  },
  bigContainer:{
      display:'flex',
      justifyContent:'center',
      alignItems:'center'
  },
  text:{
   fontSize:16,
   color:'white',
   marginTop:5
}
});
