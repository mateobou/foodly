import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View,Dimensions } from 'react-native';
import Ingredient from './Ingredient';
const vw = Dimensions.get('window').width/100;
const vh = Dimensions.get('window').height/100;
export default function Details({ingredients}) {

  return (
    <View style={styles.Container}>
        <Text>Details</Text>
    </View>
  );
}
const styles = StyleSheet.create({
    Container:{
        width:100*vw,
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'column',
        position:'relative',
        top:-30*vh
    },
    category:{
        margin:5,
        borderRadius:10,
        shadowColor:'black',
        shadowOpacity:0.1,
        shadowRadius:20, 
        backgroundColor:'white',
        padding:10
    },
    titleSelected:{
        color:'white'
    },

    CategoryeSelected:{
        margin:5,
        borderRadius:10,
        shadowColor:'black',
        shadowOpacity:0.1,
        shadowRadius:20, 
        backgroundColor:'white',
        padding:10,
        backgroundColor:'red',
        paddingHorizontal:15
    },
    title:{
        color:'black',
        fontSize:15,
        fontWeight:'bold',
        textAlign:'center'
    }
});
