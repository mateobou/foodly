import { StatusBar } from 'expo-status-bar';
import { Dimensions, Image, Pressable, StyleSheet, Text, View } from 'react-native';
import { useNavigation } from '@react-navigation/native'; 

const width = Dimensions.get('window').width/100;
const height = Dimensions.get('window').height/100;
export default function Food({content='foodTest', image, nbPersons, ingredients}) {
  const navigation = useNavigation();
  return (
    <Pressable style={styles.Food} onPress={()=>navigation.navigate('RecipeDetails', { content,image, image, nbPersons, ingredients })}>
        <Image source={{uri:image}} style={styles.img}/>
        {/*<Text style={styles.title}>{content}</Text>*/}
    </Pressable>
  );
}

const styles = StyleSheet.create({
    Food:{
      borderRadius:10,
      shadowColor:'black',
      shadowOpacity:0.1,
      shadowRadius:20, 
      backgroundColor:'white',
      padding:10,
      height:100*width/3,
      width:100*width/3,
      display:"flex",
      alignItems:"center",
      justifyContent:'center'
  },
  title:{
      color:'white',
      fontSize:15,
      fontWeight:'bold',
      textAlign:'center',
      position:'relative',
      bottom:100*width/4,
  }, 
  img:{
    width:100*width/3,
    height:100*width/3,
    resizeMode:"cover"
  }
});
