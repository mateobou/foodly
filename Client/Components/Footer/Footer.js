import {StyleSheet,View} from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import FooterContainer from './FooterContainer';
const Stack = createNativeStackNavigator();
const Footer = ()=> {
  return (
    <View style={styles.container}>
        <FooterContainer name={"test"}/>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    height:50,
    width:"100%", 
    backgroundColor:"#FFF"||'#7161ef'
  },
  gif:{
    height:300,
    width:300, 
    marginBottom:50
  }
});
export default Footer