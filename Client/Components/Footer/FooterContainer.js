import {StyleSheet,View,Text} from 'react-native';
import { Dimensions } from 'react-native-web';
const vw = Dimensions.get('window').width/100;
const vh = Dimensions.get('window').height/100; 
const FooterContainer = ({name})=> {
  return (
    <View style={styles.FooterContainer}>
        <Text>{name}</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  FooterContainer: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    width:20*vw
  },
  gif:{
    height:300,
    width:300, 
    marginBottom:50
  }
});
export default FooterContainer