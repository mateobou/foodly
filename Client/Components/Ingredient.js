import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View,Dimensions, Image } from 'react-native';
const vw = Dimensions.get('window').width/100;
const vh = Dimensions.get('window').height/100;
export default function Ingredient({title}) {
  return (
    <View  style={styles.IngredientContainer}>
       <Image source={{uri:"https://www.cuisine-de-bebe.com/wp-content/uploads/2017/05/cdbb_details-recettes-42.png"}} style={styles.image}/>
       <Text style={styles.text}>{title}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
    IngredientContainer:{
        width:80*vw,
        height:10*vh,
        backgroundColor:"#f6fff8",
        justifyContent:'flex-start',
        alignItems:'center',
        flexDirection:'row',
        borderRadius:10,
        marginVertical:5
    },
    image:{
      width:8*vh,
      marginLeft:20,
      height:8*vh
    }, 
    text:{
      marginLeft:20,
    }
});
