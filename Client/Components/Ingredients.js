import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View,Dimensions } from 'react-native';
import { FlatList } from 'react-native-web';
import Ingredient from './Ingredient';
const vw = Dimensions.get('window').width/100;
const vh = Dimensions.get('window').height/100;
export default function Ingredients({ingredients}) {
    const listeIngredients = ingredients.split('|')
    const  listeIngredientsComponent=[]

  return (
    <View style={styles.Container}>
        <Text style={styles.title}>{listeIngredients.length} items</Text>
        {listeIngredients.map((ingredient)=><Ingredient title={ingredient}/>)}
    </View>
  );
}
const styles = StyleSheet.create({
    Container:{
        width:100*vw,
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'column',
        position:'relative',
        top:-30*vh
    },
    category:{
        margin:5,
        borderRadius:10,
        shadowColor:'black',
        shadowOpacity:0.1,
        shadowRadius:20, 
        backgroundColor:'white',
        padding:10
    },
    titleSelected:{
        color:'white'
    },

    CategoryeSelected:{
        margin:5,
        borderRadius:10,
        shadowColor:'black',
        shadowOpacity:0.1,
        shadowRadius:20, 
        backgroundColor:'white',
        padding:10,
        backgroundColor:'red',
        paddingHorizontal:15
    },
    title:{
        width:80*vw,
        color:'black',
        fontSize:18,
        fontWeight:'bold',
        textAlign:'left',
        marginBottom:5,
        marginTop:30, 
    }
});
