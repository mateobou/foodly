import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View,Dimensions,Image } from 'react-native';
import Pressable from 'react-native/Libraries/Components/Pressable/Pressable';
const vw = Dimensions.get('window').width/100;
const vh = Dimensions.get('window').height/100;
export default function Mood({title,image, backgroundColor, onClick}) {
  return (
      <Pressable onPress={onClick}>
        <View  style={{...styles.MoodContainer,backgroundColor:backgroundColor}}>
            <Image source={image}/>
            <Text style={styles.text}>{title}</Text>
        </View>
        
    </Pressable>
  );
}
const styles = StyleSheet.create({
    MoodContainer:{
       width:80*vw,
       height:60*vw,
       borderRadius:15,
       marginTop:15,
       display:'flex',
       justifyContent:"center",
       alignItems:'center'
    },
    text:{
        textAlign:'center',
        marginTop:5,
        fontSize:20,
        color:'#fff',
        fontWeight:'bold',
        marginBottom:80
    }
});
