import { Dimensions, View,StyleSheet, Text } from "react-native";
import {useState } from "react";
import OptionBarCategory from "./OptionBarCategory";
import Ingredients from "./Ingredients";
import Details from "./Details";
const vw = Dimensions.get('window').width/100;
const vh = Dimensions.get('window').height/100; 
export default function OptionBar({ingredients}) {
    const [detailsisSelected, updateDetailsComponent] = useState(true)
    const [ingredientsIsSelected, updateIngredients] = useState(false)
    function onClick(element){

        if(element==="Details"){
            updateDetailsComponent(true) 
            updateIngredients(false)
        }  
        else{
            updateDetailsComponent(false) 
            updateIngredients(true)
        }
    }
  return (
      <>
        <View style={styles.Container}>
            <View style={styles.OptionBar}>
                    <OptionBarCategory style={styles.Category} selected={detailsisSelected} text="Details" onClick={(element)=>onClick(element)}/>
                    <OptionBarCategory style={styles.Category} selected={ingredientsIsSelected} text="ingredients" onClick={(element)=>onClick(element)}/>
                </View>
        </View>
        {detailsisSelected ? <Details/> : <Ingredients ingredients={ingredients}/>}
        
      </>
  
  );
}

const styles = StyleSheet.create({
  OptionBar: {
    width:80*vw,
    borderRadius:100,
    backgroundColor:"#f6fff8",
    height:7*vh,
    display:"flex",
    flexDirection:"row",
    justifyContent:"center",
    alignItems:'center', 
    position:'relative',
    top:-32*vh
  },
  Container:{
      width:100*vw,
      display:'flex',
      justifyContent:"center",
      alignItems:'center'
  },
  color:{
      color:'white'
  },
  Category:{
    width:40*vw, 
    justifyContent:"center",
    alignItems:'center'
}
});
