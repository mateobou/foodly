import { LinearGradient } from "expo-linear-gradient";
import { Dimensions, View,StyleSheet, Text } from "react-native";
import Pressable from "react-native/Libraries/Components/Pressable/Pressable";
    const vw = Dimensions.get('window').width/100;
    const vh = Dimensions.get('window').height/100; 
export default function OptionBarCategory({selected=false,text,onClick}) {
    
  return (
      <Pressable onPress={()=>onClick(text)}>
        <LinearGradient colors={selected==true ? ["#e60c5b","#ff8080"]: ['#f6fff8','#f6fff8']} style={selected===true ? styles.Selected:  styles.Category}>
            <Text style={selected ? {color:'white'}: {color:'black'}}>{text}</Text>
        </LinearGradient>
    </Pressable>                
  
  );
}

const styles = StyleSheet.create({

  Category:{
    width:38*vw, 
    justifyContent:"center",
    alignItems:'center',
    height:6*vh,
    borderRadius:15
},
Selected:{
    width:38*vw, 
    justifyContent:"center",
    alignItems:'center',
    height:6*vh,
    borderRadius:15,
    shadowColor:'black',
    shadowOpacity:0.1,
    shadowRadius:20, 
}
});
