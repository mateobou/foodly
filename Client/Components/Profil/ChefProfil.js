
import { StyleSheet, Text, View, Image } from 'react-native';
import ChefImage from "./../../images/Chef1.png"
export default function Chef({content="Moi"}) {
  return (
    <View style={styles.container}>
        <Image source={ChefImage}/>
        <Text style={styles.title}>{content}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
   fontSize:16,
   width:60, 
   fontWeight:"bold",
   textAlign:'left',
   color:'#000',
   textAlign:'center'
  },
  container:{
    display:'flex',
    justifyContent:'flex-start',
    alignItems:'center',
  }
});
