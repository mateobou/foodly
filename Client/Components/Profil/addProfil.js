
import { StyleSheet, Text, View, Image } from 'react-native';
export default function AddChef({content="Moi"}) {
  return (
    <View style={styles.container}>
        
        <Text style={styles.title}>+</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
   fontSize:16,
   width:60, 
   fontWeight:"bold",
   textAlign:'left',
   color:'#FFF',
   textAlign:'center'
  },
  container:{
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    width:65,
    height:65,
    borderRadius:20,
    backgroundColor:'#D9D9D9'
  }
});
