import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import {Pressable} from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { useState, useEffect } from 'react';
import FoodJson from './../../YouCook.json'
export default function AddRecipe({navigation, onPress}) {
    
  return (
    
        <LinearGradient colors={["#F6AD9F","#F8ABBD"]} style={styles.Container}>
            <Pressable onPress={onPress}>
                <Text style={styles.text}>Add a recipe</Text>
            </Pressable>
            
        </LinearGradient>

  );
}

const styles = StyleSheet.create({
  Container:{
    width:200,
    height:250,
    borderRadius:20, 
    display:"flex",
    justifyContent:'center',
    alignItems:'center'
  },
  text:{
    color:'white',
    fontSize:16,
    fontWeight:"bold"
  }
});
