
import React, { useEffect, useState } from 'react';
import { StyleSheet, TextInput, View } from 'react-native'
import Data from './../YouCook.json'
function SearchInput() {

    const [Tab, setTab] = useState([]);
    const [text, ChangeText] = React.useState('');
    function searchFor(text) {
      let Tableau = [];
      Data.map((recipe) => {
        if (recipe.RecipeName.search(text) > -1) {
          console.log(text)
          Tableau.push(recipe);
        }
        return text;
      }, Tableau)
      return Tableau;
    }
  return (
    
    <View style={styles.container}>
         <TextInput
        style={styles.input}
        value={text}
        placeholder="Search your perfect recipe"
        onChangeText={e => {
          setTab([]);
          ChangeText(e);;
          setTab([])
          setTab([...searchFor(text)]);
        }}
      />
    </View>
  );
}
const styles = StyleSheet.create({
    input: {
        height: 50,
        margin: 12,
        backgroundColor:'white',
        padding: 10,
        width:350,
        borderRadius:12,
        shadowColor:'black',
        shadowOpacity:0.1,
        shadowRadius:12, 
        color:'grey'
  },
  Text:{
    marginTop:15,
    width:300,
    fontWeight:'bold',
    fontSize:22,
  }
});
export default SearchInput