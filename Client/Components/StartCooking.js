import { Button, Pressable, StyleSheet, Text, View } from 'react-native';

export default function StartCooking() {
  return (
    <Pressable title="Get Started" style={styles.button} color='#f1c0e8'>
            <Text style={styles.color}>
            Start Cooking
            </Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  button: {
    height:35,
    width:150,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:10,
    marginTop:25,
    backgroundColor:"white",
    color:'black'
  },
  color:{
      color:'black',
      fontSize:12,
      fontWeight:"bold"
  }
});
