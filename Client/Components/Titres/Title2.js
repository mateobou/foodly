import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function Title3({content, color="#FFF"}) {
  return (
    <Text style={{...styles.title,color}}>{content}</Text>
  );
}

const styles = StyleSheet.create({
  title: {
   fontSize:25,
   fontWeight:'bold',
   width:300, 
   textAlign:'left',

  },
});
