import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function Title2({content}) {
  return (
    <Text style={styles.title}>{content}</Text>
  );
}

const styles = StyleSheet.create({
  title: {
   fontSize:22,
   fontWeight:'bold',
   width:300, 
   textAlign:'left',
   color: "#000"
  },
});
