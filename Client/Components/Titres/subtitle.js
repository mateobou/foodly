import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function Subtitle({content,color="#dec0f1"}) {
  return (
    <Text style={{...styles.title,color:color}}>{content}</Text>
  );
}

const styles = StyleSheet.create({
  title: {
   fontSize:16,
   width:300, 
   fontWeight:"bold",
   textAlign:'left',
  },
});
