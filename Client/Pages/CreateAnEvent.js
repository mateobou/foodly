import { StyleSheet } from "react-native";
import { useState } from "react";
import Title from "../Components/Titres/Title";
import { View } from "react-native";
import Subtitle from "../Components/Titres/subtitle";
import Chef from "../Components/Profil/ChefProfil";
import AddRecipe from "../Components/Recettes/AddRecette";
import Title2 from "../Components/Titres/Title2";
import { useEffect } from "react";
import FoodJson from './../YouCook.json'
import { Dimensions } from "react-native";
import Title3 from "../Components/Titres/Title3";
import AddChef from "../Components/Profil/addProfil";
import BoutonCentre from "../Components/Boutons/BoutonCentered";
const vw = Dimensions.get('window').width/100;
const vh = Dimensions.get('window').height/100;
function Event({navigation}) {
    const [Recettes,updateRecettes] = useState()
    useEffect(()=>{
        fetch("http://localhost:3001/findYouCook")
        .then(response => response.json())
        .then(response => updateRecettes(response))
        .then(()=>console.log(Recettes));
        if(!Recettes)
        {
        updateRecettes([...FoodJson])
        }
        return ()=>console.log(Recettes)
    },[])
  return (
    <View style={styles.bigContainer}>
        <View style={styles.container}>
            <Title2 content={"Anniversaire de Charlotte"} color="#000"/>
            <Title3 content={"Participants"} color="#000"/>
            <View style={styles.chefContainer}>
                <Chef/>
                <AddChef/>            
            </View>
            <Title3 content="Menu" color="#FFF"/>
            <AddRecipe onPress={()=>navigation.navigate('MainPage', { Recettes:{...Recettes} })}/>
            <View style={styles.lineFooter}>
                <BoutonCentre title='Ajouter la recette'/>
            </View>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  bigContainer:{
    width:100*vw,
    height:100*vh,
    display:'flex',
    alignItems:'center',
    backgroundColor: '#FFF',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'flex-start',
    justifyContent: 'center',
    width:90*vw
  },
  gif:{
    height:250,
    width:250, 
    borderRadius:250,
    marginBottom:50,
  },
  chefContainer:{
    display:'flex',
    flexDirection:'row',
  }, 
  lineFooter:{
    width:100*vw,
    display:"flex",
    justifyContent:'center',
    alignItems:'center',
    position:'fixed',
    bottom:-10
  }
});
export default Event