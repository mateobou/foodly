import { useEffect, useState } from 'react';
import { Dimensions, ScrollView, StyleSheet, Text, View } from 'react-native'
import BoutonCentre from '../Components/Boutons/BoutonCentered';
import Category from '../Components/Category';
import Food from '../Components/Food';
import SearchInput from '../Components/Search';
import FoodJson from './../food.json'

const vw = Dimensions.get('window').width/100;
const vh = Dimensions.get('window').height/100;
function MainPage({ route, navigation }) {
  const [arrayFood,UpdatearrayFood] = useState([])
  const Recipe = [];
  const { Recettes } = route.params;
  useEffect(()=>{
    if(!Recettes)
    {
      updateRecettes([...FoodJson])
    }
    let i = 0;
    for (const property in Recettes) {
      Recipe.push(<Food content={`${Recettes[property].RecipeName}`} image={`${Recettes[property].IMG}`} key={i} ingredients={`${Recettes[property].Ingredients_}`} nbPersons={`${Recettes[property].NBpersons_}`}/>)
      i++;
    }
    UpdatearrayFood([...Recipe])
  },[])
  return (
    <View style={styles.container}>
        <Text style={styles.Text}>Easy to cook menu</Text>
        <ScrollView>
          <SearchInput/>
          <View>
              <Text style={styles.Text}>Category</Text>
              <View style={styles.line}>
                <Category content={"All"} status="selected"/>
                <Category content={"Breakfast"}/>
                <Category content={"Asian food"}/>
              </View>
              <View style={styles.food}>
                {arrayFood.sort().map((recipe)=> recipe)}
              </View>
          </View>
        </ScrollView>
        <View style={styles.lineFooter}>
          <BoutonCentre title='Prendre une photo'/>
        </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    display:'flex',
    alignItems:'center'
  },
  Text:{
    marginTop:15,
    width:340,
    fontWeight:'bold',
    fontSize:22,
    marginLeft:20
  },
  line:{
    display:'flex',
    flexDirection:'row',
    marginLeft:20
  },
  food:{
    display:'flex',
    flexDirection:'row',
    flexWrap:'wrap'
  },
  lineFooter:{
    width:100*vw,
    height:75,
    display:"flex",
    justifyContent:'center',
    alignItems:'center',
    position:'fixed',
    backgroundColor:'white'
  }
  
});
export default MainPage