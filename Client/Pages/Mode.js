import { StatusBar } from 'expo-status-bar';
import { useEffect, useState } from 'react';
import { Button, Dimensions, ScrollView, StyleSheet, Text, View } from 'react-native'
import Mood from '../Components/Mood';
import Chef from './../images/Chef.png'
import Dinner from './../images/Dinner.png'
const vw = Dimensions.get('window').width/100;
const vh = Dimensions.get('window').height/100;
function ModePage({ route, navigation }) {
    const { Recettes } = route.params;
  return(
    <View>
      <Text style={styles.text}>Mood</Text>
      <View style={styles.container}>
        <View style={styles.column}>
          <Mood title={"J'organise"} backgroundColor={"#E8998D"} image={Chef} onClick={()=>navigation.navigate('Event', { Recettes:{...Recettes} })}/>

        </View>
        <View style={styles.column}>
          <Mood title={"Je rejoins"} backgroundColor={"#F0C808"} image={Dinner}/>
        </View>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    display:'flex',
    justifyContent:"center",
    alignItems:'center',
    flexDirection:'column'
  },
  column:{
    width:50*vw,
    display:'flex',
    justifyContent:"center",
    alignItems:'center',
  },
  text:{
    fontWeight:'bold',
    textAlign:'center',
    fontSize:22,
    marginTop:10
  }
});
export default ModePage