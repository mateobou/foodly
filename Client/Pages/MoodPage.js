import { StatusBar } from 'expo-status-bar';
import { useEffect, useState } from 'react';
import { Button, Dimensions, ScrollView, StyleSheet, Text, View } from 'react-native'
import Mood from '../Components/Mood';
const vw = Dimensions.get('window').width/100;
const vh = Dimensions.get('window').height/100;
function MoodPage({ route, navigation }) {
  return(
    <View>
      <Text style={styles.text}>Mood</Text>
      <View style={styles.container}>
        <View style={styles.column}>
          <Mood title={"Mood booster"} backgroundColor={"#E8998D"}/>
        </View>
        <View style={styles.column}>
          <Mood title={"Brain Focus"} backgroundColor={"#F0C808"}/>
        </View>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    display:'flex',
    justifyContent:"center",
    alignItems:'center',
    flexDirection:'row'
  },
  column:{
    width:50*vw,
    display:'flex',
    justifyContent:"center",
    alignItems:'center',
  },
  text:{
    fontWeight:'bold',
    textAlign:'center',
    fontSize:22,
    marginTop:10
  }
});
export default MoodPage