import { Button, Dimensions, Image, StyleSheet, Text, View, ScrollView } from 'react-native'
import BoutonGetStarted from '../Components/Boutons/Bouton';
import BoutonCentre from '../Components/Boutons/BoutonCentered';
import CookingEmbed from '../Components/CookingEmbed';
import OptionBar from '../Components/OptionBar';

const vw = Dimensions.get('window').width/100;
const vh = Dimensions.get('window').height/100;
function RecipeDetail({ route, navigation }) {
    const {content,image, nbPersons, ingredients} = route.params
  return(
    <View>
      <ScrollView>
          <CookingEmbed title={content} image={image}/>
          <OptionBar ingredients={ingredients}/>
      </ScrollView>
      <View style={styles.lineFooter}>
        <BoutonCentre title='Ajouter la recette'/>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(196, 196, 196, 0.5)',
    display:'flex',
    alignItems:'center'
  },
  Text:{
    marginTop:15,
    width:340,
    fontWeight:'bold',
    fontSize:22,
    marginLeft:20
  },
  line:{
    display:'flex',
    flexDirection:'row',
    marginLeft:20
  },
  food:{
    display:'flex',
    flexDirection:'row',
    flexWrap:'wrap',
    justifyContent:'space-around'
  },
  lineFooter:{
    width:100*vw,
    display:"flex",
    justifyContent:'center',
    alignItems:'center',
    position:'fixed',
    bottom:30
  }
});
export default RecipeDetail