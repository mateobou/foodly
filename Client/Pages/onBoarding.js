
import { useEffect, useState } from 'react';
import { Button, Image, StyleSheet, Text, View } from 'react-native';
import BoutonGetStarted from './../Components/Boutons/Bouton';
import Subtitle from '../Components/Titres/subtitle';
import Title from '../Components/Titres/Title';
import GIF from './../images/food.gif';
import FoodJson from './../YouCook.json'
function OnBoarding({navigation}) {
  const [Recettes,updateRecettes] = useState()
  useEffect(()=>{
    fetch("http://localhost:3001/findYouCook")
    .then(response => response.json())
    .then(response => updateRecettes(response))
    .then(()=>console.log(Recettes));
    if(!Recettes)
    {
      updateRecettes([...FoodJson])
    }
    return ()=>console.log(Recettes)
  },[])
  return (
    
    <View style={styles.container}>
      <Image source={GIF} style={styles.gif}/>
      <Subtitle content={"+16K premium recipes"}/>
      <Title content={"Cook like a chef"}/>
      <BoutonGetStarted onClick={()=>navigation.navigate('Mode', { Recettes:{...Recettes} })}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7161ef',
    alignItems: 'center',
    justifyContent: 'center',
  },
  gif:{
    height:250,
    width:250, 
    borderRadius:250,
    marginBottom:50,
  }
});
export default OnBoarding